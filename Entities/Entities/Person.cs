﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public abstract class Person
    {
		private string lastName;
		private int dni;
		private int edad;
		private string name;

		protected Person(string name, string lastName, int edad, int dni)
		{
			this.lastName = lastName;
            this.dni = dni;
			this.edad = edad;
            this.name = name;
		}

		public string GetName() {
			return name;
		}

		public void SetName(string name) {
			this.name = name;
		}

		public string GetLastName()
		{
			return lastName;
		}

		public void SetLastName(string name)
		{
			this.lastName = name;
		}

		public int GetDni() {
			return dni;
		}

		public void SetDni(int n)
		{
			dni = n;
		}

		public void SetEdad(int n)
		{
			edad = n;
		}

		public int GetEdad()
		{
			return edad;
		}

		public virtual string Mostrar()
		{
			StringBuilder stringBuilder = new StringBuilder();
			return stringBuilder.
				AppendFormat("Nombre: {0} Apellido: {1} Edad: {2} DNI: {3}", name, lastName, edad, dni).ToString();

		}

		public abstract bool Validate();
	}
}
