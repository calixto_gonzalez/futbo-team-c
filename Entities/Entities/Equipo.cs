﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Equipo
    {
        private const int cantidadMaximaDeJugadores = 6;
        private List<Jugador> jugadores;
        private readonly string nombre;
        private DirectorTecnico directorTecnico;

        public DirectorTecnico DirectorTecnico
        {
            get { return directorTecnico; }
            set { directorTecnico = value; }
        }


        private Equipo()
        {
            jugadores = new List<Jugador>();

        }

        public Equipo(string nombre) : this()
        {
            this.nombre = nombre;
            
        }

        public string GetNombre()
        {
            return this.nombre;
        }


        public static explicit operator string(Equipo e)
        {
          
            StringBuilder listaJugadoresYDt = new StringBuilder();
            string dt;

            listaJugadoresYDt.AppendLine("Jugadores");
            if (e.jugadores != null){
            foreach (Jugador jugador in e.jugadores)
            {
                listaJugadoresYDt.AppendLine(jugador.Mostrar());
            }
            }


            if (e.directorTecnico == null)
            {
                dt = "Dt sin asignar";
            }

            else
            {
                dt = e.directorTecnico.Mostrar();
            }

            return listaJugadoresYDt.AppendLine(dt).ToString();
        }

        public static bool ValidarEquipo(Equipo e)
        {
            bool hayDefensor = false;
            bool hayDelantero = false;
            bool hayCentral = false;
            int arquero = 0;

            if (e.jugadores == null)
            {
                return false;
            }
 
            foreach (Jugador jugador in e.jugadores)
            {
                if (jugador.Posicion == Posicion.Arquero)
                {
                    arquero += 1;
                }
                if (jugador.Posicion == Posicion.Defensor ) {
                    hayDefensor = true;
                }
                if (jugador.Posicion == Posicion.Delantero ) {
                    hayDelantero = true;
                }
                if (jugador.Posicion == Posicion.Central ) {
                    hayCentral = true;
                }
            }
            

            if (e.directorTecnico == null || e.jugadores.Count < 1
                                          || arquero != 1
                                          || Equipo.cantidadMaximaDeJugadores != e.jugadores.Count
                                          || hayDefensor == false
                                          || hayCentral == false
                                          || hayDelantero == false
                                          )
            {
                return false;
            }

            return true;


        }

        public static bool operator ==(Equipo e, Jugador j)
        {
            if (j == null)
            {
                return false;
            }
            return e.jugadores.Contains(j);
        }

        public static bool operator !=(Equipo e, Jugador j)
        {
            return !(e == j);
        }

        public static Equipo operator +(Equipo e, Jugador j)
        {
            if (e.jugadores == null || e == null || j == null)
            {
                return e;
            }
            if (e == j || e.jugadores.Count >= cantidadMaximaDeJugadores || !(j.Validate()))
            {
                return e;
            }
            e.jugadores.Add(j);
            return e;
        }
      
    }
}

