﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
	public class Jugador : Person
	{
		private float altura;
		private float peso;
        public Posicion Posicion { get; }

        public Jugador(string name, string lastName, int edad, int dni, float peso, float altura, Posicion posicion) : base(name, lastName, edad, dni)
		{
			this.altura = altura;
			this.peso = peso;
			this.Posicion = posicion;
		}

		public override string Mostrar()
		{
			return base.Mostrar();
		}

		public override bool Validate()
		{
			return  GetEdad() < 40 && ValidateEstadoFisico();
		}

        public bool ValidateEstadoFisico()
		{
			double estadoFisico = peso / Math.Pow(altura, 2);
			return estadoFisico >= 18.5 && estadoFisico <= 25;
		}
	}
}
