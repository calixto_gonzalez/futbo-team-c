﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
	public class DirectorTecnico : Person
	{
		private int yearsOfExpercience;

		public DirectorTecnico(string name, string lastName, int edad, int dni, int yearsOfExpercience) : base(name, lastName, edad, dni)
		{
			this.yearsOfExpercience = yearsOfExpercience;
		}

		public int GetYearsOfExpercience() {
			return yearsOfExpercience;
		}

		public override string Mostrar()
		{
			StringBuilder toString = new StringBuilder(base.Mostrar());
			return toString.AppendFormat("Anos de experciencia: {0}",yearsOfExpercience).ToString();
		}

		public override bool Validate()
		{
			return yearsOfExpercience >= 2 && GetEdad() < 65;
		}

	}
}
